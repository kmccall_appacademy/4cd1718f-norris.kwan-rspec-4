class Book
  attr_reader :title

  LOWERCASE_WORDS = ['the', 'a', 'an', 'in', 'if', 'of', 'and']

  def title=(title)
    words = title.downcase.split
    results = words.map.each_with_index do |word, i|
      if LOWERCASE_WORDS.include?(word) && !i.zero?
        word
      else
        word.capitalize
      end
    end
    @title = results.join(' ')
  end
end
