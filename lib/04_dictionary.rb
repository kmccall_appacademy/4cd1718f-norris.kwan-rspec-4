class Dictionary
  attr_accessor :entries

  def initialize(entry = {})
    @entries = entry
  end

  def add(entry)
    if entry.is_a? Hash
      @entries.merge!(entry)
    elsif entry.is_a? String
      @entries[entry] = nil
    end
  end

  def find(query)
    @entries.select do |word|
      word.match(query)
    end
  end

  def include?(word)
    @entries.keys.include?(word)
  end

  def keywords
    @entries.keys.sort
  end

  def printable
    result = keywords.map do |word|
      "[#{word}] \"#{@entries[word]}\""
    end
    result.join("\n")
  end
end
