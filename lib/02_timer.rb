class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    time = {
      hour: (@seconds / 60 / 60),
      min: (@seconds / 60 % 60),
      sec: (@seconds % 60)
    }
    result = time.values.map do |val|
      val < 10 ? "0#{val}" : val.to_s
    end
    result.join(':')
  end
end
